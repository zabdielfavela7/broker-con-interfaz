import sqlite3
from sqlite3 import Error

import tkinter as tk
import json
import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish
import time as t
t.sleep(10)
from PIL import ImageTk,Image
import multiprocessing
import os
import requests
from datetime import datetime
from subprocess import check_output
'''
#-----------------------------------------------------------------------------------------------------------------------------------------------
#funcion para leer los mensajes de
#el topico y guardarlos en la base
#de datos
def messages():
        waiting=10 #tiempo de espera entre ciclos
        conn = sqlite3.connect('data_base.db') #conexion a la base de datos
        db = conn.cursor()

        while True:
                try:
                        MQTT_SERVER = "192.168.100.9" #ip del broker
                        MQTT_PATH = "uploads" #nombre del topico donde se publicaran los mensajes

                        # The callback for when the client receives a CONNACK response from th                                                                                      e server.
                        def on_connect(client, userdata, flags, rc):
                                print("Connected with result code " + str(rc))
                                client.subscribe(MQTT_PATH, qos=2)      #Quality of Service 2
                        def on_message(client, userdata, msg):
                                mensaje = str(msg.payload)
                                print (mensaje)
                                db.execute("INSERT INTO tabla (status, message) values (?, ?)",(0, mensaje))
                                conn.commit()
                        client = mqtt.Client(client_id="broker_1", clean_session=False)
                        client.on_connect = on_connect
                        client.on_message = on_message
                        client.connect(MQTT_SERVER, 1883, 60)
                        client.loop_forever()
                except:
                        print ("fallo total")
                        #print e
                        pass

#-----------------------------------------------------------------------------------------------------------------------------------------------------


def data_base_upload():
	waiting=10
	conn = sqlite3.connect('data_base.db')
	db = conn.cursor()
	address="http://3.129.0.80:4000/b/"
	#address="192.168.100.11/b/"
	while True:
		try:
			array = []
			db.execute('SELECT message FROM tabla WHERE status = ? LIMIT 10', (0, ))
			value=db.fetchall()
			conn.commit()
			data = json.dumps(value)

			#sacamos todos los valores y los convertimos uno por uno a json para agregarlo a un arreglo
			for x in range (0,len(value)):
				mssg=json.loads((str(value[x]))[3:-3])
				array.append(mssg)

			data=json.dumps(array)
                        #print (array)
			flag=0
			while flag==0:
                                #print data
				res = requests.post(address, data=data)
                                #print (res.status_code)
				if res.status_code == 200:
					length= len(array)
                                        #print length

					for x in range (0,length):
						#print json.dumps(array[x])
                                                new_value=(str(json.dumps(array[x])))
                                                print (new_value)
                                                db.execute('UPDATE tabla SET status = 1 WHERE message = ?', (new_value, ))
                                                conn.commit()

					flag=1
				t.sleep(10)

		except Error as e:
			print (e)
			print ("se acabo")
			t.sleep(10)
'''
#-----------------------------------------------------------------------------------------------------------------------------------------------


alturaa=2
ruta_background="/home/pi/Desktop/FarmeaseScale.png"
class App(tk.Tk):
    def __init__(self):
        tk.Tk.__init__(self)

        def deleted():
                k=self.focus_get()
                print (k)
                if k in [self.e1]:
                                self.e1.delete(0, tk.END)
                                print ("borre")
                if k in [self.e2]:
                                self.e2.delete(0, tk.END)
                                print ("borre")
                if k in [self.e3]:
                                self.e3.delete(0, tk.END)
                                print ("borre")
                if k in [self.e4]:
                                self.e4.delete(0, tk.END)
                                print ("borre")

        def getInput():
                print("q")
                global message
                global valor
                t.sleep(.5)
                unidad_maximo=self.e1.get()
                unidad_minimo=self.e2.get()
                caja_maximo=self.e3.get()
                caja_minimo=self.e4.get()

                print (unidad_maximo)

                if float(unidad_maximo)>float(unidad_minimo):
                        if float(caja_maximo)>float(caja_minimo):
                                if float(unidad_maximo)<float(caja_minimo):
                                        broker="192.168.100.9"
                                        port=1883
                                        topic = "rangos"
                                        def on_publish(client,userdata,mid):
                                                print (mid)
                                                if mid==1:
                                                        print ("caja subida")
                                                        mid=0
                                        data = (caja_minimo+","+caja_maximo+","+unidad_minimo+","+unidad_maximo)
                                        client1= mqtt.Client("boss") #create client object
                                        client1.on_publish = on_publish #assign function to callback
                                        client1.connect(broker,port) #establish connection
                                        ret= client1.publish(topic, data, retain=True) #publish

                                        self.e1.delete(0, tk.END)
                                        self.e2.delete(0, tk.END)
                                        self.e3.delete(0, tk.END)
                                        self.e4.delete(0, tk.END)
                                        print ("error")
                                        #label3.destroy(label3)
                                else:
                                        #global f
                                        print ("error")
                                        t.sleep(.1)
                                        message=tk.Frame(self)
                                        message.place(relx=0, rely=0.15)
                                        print (message)
                                        mensaje=(tk.Label(message, text="hello world3", bg="red").pack())
                        else:
                                #global f
                                print ("error")
                                t.sleep(.1)
                                message=tk.Frame(self)
                                message.place(relx=0.03, rely=0.6)
                                print (message)
                                mensaje=(tk.Label(message,font="Arial 13",fg="white", text="ERROR: maximo menor al minimo", bg="red").pack())
                else:
                        #global f
                        print ("error")
                        t.sleep(.1)
                        message=tk.Frame(self)
                        message.place(relx=0.03, rely=0.2)
                        print (message)
                        mensaje=(tk.Label(message,font="Arial 13",fg="white", text="ERROR: maximo menor al minimo", bg="red").pack())
                        #canvas.tag_raise(message)
                        #lop=message.pack(anchor="n")

                label1 = tk.Label(self, text=caja_maximo+" lb")
                label1.place(relx=.29,rely=.9)

                #print min value box
                label1 = tk.Label(self, text=caja_minimo+" lb")
                label1.place(relx=.29,rely=.8)

                #print max value unit
                label1 = tk.Label(self, text=unidad_maximo+" lb")
                label1.place(relx=.29,rely=.5)

                #print max value unit
                label1 = tk.Label(self, text=unidad_minimo+" lb")
                label1.place(relx=.29,rely=.4)
                x=5
                return x

        def sequence(*functions):
            def func(*args, **kwargs):
                return_value = None
                for function in functions:
                    return_value = function(*args, **kwargs)
                return return_value
            return func

        def clean_up():
            try:
                message.destroy()
            except:
                pass

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------
        self.geometry('800x470')
        self.config(cursor="dot")
        image = Image.open(ruta_background)
        photo_image = ImageTk.PhotoImage(image)
        label = tk.Label(self, image = photo_image, text="titi")
        label.pack()
        self.title("Configuracion de rangos Farmease")

        tittle=tk.Label(self, justify="center",font="Arial 25 bold", bg="#AFD1EA" ,text="Configuracion de rangos").place(relx=.5,rely=.05, anchor="c")
        #titulo.lift()

        #########     UNIDAD
        label2 = tk.Label(self, font="Arial 15 bold", text="Rangos de unidad").place(relx=.03,rely=.3)
        c2=self.e2 = tk.Entry(self, bd=2, width=18)
        c2.place(relx=.03,rely=.4)
        label2 = tk.Label(self, text="MIN:").place(relx=.23,rely=.4)

        c1=self.e1 = tk.Entry(self, bd=2, width=18)
        c1.place(relx=.03,rely=.5)
        label1 = tk.Label(self, text="MAX:").place(relx=.23,rely=.5)


        #########     CAJA
        label2 = tk.Label(self, font="Arial 15 bold", text="Rangos de caja").place(relx=.03,rely=.7)
        c4=self.e4 = tk.Entry(self, bd=2, width=18)
        c4.place(relx=.03,rely=.8)
        label4 = tk.Label(self, text="MIN:").place(relx=.23,rely=.8)

        c3=self.e3 = tk.Entry(self, bd=2, width=18)
        c3.place(relx=.03,rely=.9)
        label3 = tk.Label(self, text="MAX:").place(relx=.23,rely=.9)

        keypad_frame = tk.Frame(self)
        keypad_frame.place(relx=0.48, rely=0.2)

        alto=3
        ancho=13
        fuente="Arial 11 bold"
        tk.Button(keypad_frame, font=fuente, text="1", height = alto, width = ancho, bg='grey', fg='white', command=lambda: self.set_text("1")).grid(row=1, column=3)
        tk.Button(keypad_frame, font=fuente, text="2", height = alto, width = ancho, bg='grey', fg='white', command=lambda: self.set_text("2")).grid(row=1, column=4)
        tk.Button(keypad_frame, font=fuente, text="3", height = alto, width = ancho, bg='grey', fg='white', command=lambda: self.set_text("3")).grid(row=1, column=5)

        tk.Button(keypad_frame, font=fuente, text="4", height = alto, width = ancho, bg='grey', fg='white', command=lambda: self.set_text("4")).grid(row=2, column=3)
        tk.Button(keypad_frame, font=fuente, text="5", height = alto, width = ancho, bg='grey', fg='white', command=lambda: self.set_text("5")).grid(row=2, column=4)
        tk.Button(keypad_frame, font=fuente, text="6", height = alto, width = ancho, bg='grey', fg='white', command=lambda: self.set_text("6")).grid(row=2, column=5)

        tk.Button(keypad_frame, font=fuente, text="7", height = alto, width = ancho, bg='grey', fg='white', command=lambda: self.set_text("7")).grid(row=3, column=3)
        tk.Button(keypad_frame, font=fuente, text="8", height = alto, width = ancho, bg='grey', fg='white', command=lambda: self.set_text("8")).grid(row=3, column=4)
        tk.Button(keypad_frame, font=fuente, text="9", height = alto, width = ancho, bg='grey', fg='white', command=lambda: self.set_text("9")).grid(row=3, column=5)

        tk.Button(keypad_frame, font=fuente, text="0", height = alto, width = ancho, bg='grey', fg='white', command=lambda: self.set_text("0")).grid(row=4, column=4)
        tk.Button(keypad_frame, font=fuente, text=".", height = alto, width = ancho, bg='grey', fg='white', command=lambda: self.set_text(".")).grid(row=4, column=3)
        tk.Button(keypad_frame, font=fuente, text="CLEAR", height = alto, width = ancho, bg='grey', fg='white', command=deleted).grid(row=4, column=5)

        tk.Button(keypad_frame, font=fuente, text="ENTER", height = alto, width = 46, bg='grey', fg='white',command = sequence(clean_up, getInput)).grid(row=5, column=3, columnspan=5)



        self.mainloop()


    def set_text(self, text):
        widget = self.focus_get()
        print (widget)
        if widget in [self.e1, self.e2, self.e3, self.e4]:
            l=widget.insert("insert", text)
            print (self.e1)

if __name__  == "__main__":
    '''
    p1 = multiprocessing.Process(target=messages)
    p2 = multiprocessing.Process(target=data_base_upload)

    p1.start()
    p2.start()
    '''
    App()
