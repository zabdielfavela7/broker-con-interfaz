'''
programa para leer mensajes del topico y gurdarlos en una base
de datos con un numero de status 1=enviado al server y 0=no enviado
al server, ademas de tener un subproceso para enviar datos cada n
tiempo
'''
import sqlite3
from sqlite3 import Error
import multiprocessing
import paho.mqtt.publish as publish
import os
import time as t
#t.sleep(10)
import requests
from datetime import datetime
import json
import paho.mqtt.client as mqtt
from subprocess import check_output



# subproceso para leer los mensajes del topico y almacenarlo en la base de datos
def messages():
	waiting=10
        conn = sqlite3.connect('data_base.db')
        db = conn.cursor()

	while True:
		try:
			MQTT_SERVER = "192.168.100.9"
			MQTT_PATH = "uploads"

			# The callback for when the client receives a CONNACK response from th                                                                                      e server.
			def on_connect(client, userdata, flags, rc):
				print("Connected with result code " + str(rc))
				client.subscribe(MQTT_PATH, qos=2)      #Quality of Service 2

			def on_message(client, userdata, msg):

				mensaje = str(msg.payload)

				print (mensaje)
				db.execute("INSERT INTO tabla (status, message) values (?, ?)",(0, mensaje))
	                        conn.commit()
	                        print ("guardado")
	                        t.sleep (1)


			client = mqtt.Client(client_id="broker_1", clean_session=False)
			client.on_connect = on_connect
			client.on_message = on_message

			client.connect(MQTT_SERVER, 1883, 60)
			client.loop_forever()

		except Error as e:
			print ("fallo total")
			print e
			pass

#-----------------------------------------------------------------------------------------------------------------------------------------------------

#subproceso para enviar 10 o menos mensajes empaquetados en un array hacia el server
def data_base_upload():

        waiting=10
        conn = sqlite3.connect('data_base.db')
        #db = conn.cursor()

        address="http://3.129.0.80:4000/b/"

        while True:
		try:
			array = []
			db = conn.cursor()
			db.execute('SELECT message FROM tabla WHERE status = ? LIMIT 10', (0, ))
			value=db.fetchall()
			#array.append(value)
			#print array
			#print value
			conn.commit()
			#conn.close()
			#print len(value)
			data =json.dumps(value)
			#print (data)

			for x in range (0,len(value)):
				print (str(value[x]))
				mssg=json.loads((str(value[x]))[3:-3])
				print (mssg)
				array.append(mssg)
			data=json.dumps(array)
			print (array)
			flag=0
			while flag==0:
				#print data
				res = requests.post(address, data=data)
				print (res.status_code)
				if res.status_code == 200:
                        		length= len(array)
                        		print length
					db = conn.cursor()
					for x in range (0,length):
						#print json.dumps(array[x])
						new_value=(str(json.dumps(array[x])))
						print (new_value)
                                		db.execute('UPDATE tabla SET status = 1 WHERE message = ?', (new_value, ))
                                		conn.commit()
					#conn.close()
					flag=1
			t.sleep(8)
			#conn.close()
		except Error as e:
			#conn.close()
			print str(e)
			print "se acabo"
			t.sleep(60)

if __name__ == "__main__":

	p1 = multiprocessing.Process(target=data_base_upload)
        p2 = multiprocessing.Process(target=messages)

	p1.start()
	p2.start()
